import {
  UPDATE,
  UPDATED_SETTINGS,
  UPDATING_SETTINGS
} from '../actions/user.js';

const user = (state = {}, action) => {
  switch (action.type) {
    // Any navigation should reset the checkout form.
    case UPDATE:
      return action.user;

    case UPDATED_SETTINGS :
    return {
      ...state,
      settings : action.settings
    }

    default:
      return state;
  }
}

export default user;
