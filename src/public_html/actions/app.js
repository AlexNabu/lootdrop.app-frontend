/**
 * @license
 * Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { fetchCategoryItemsIfNeeded, fetchCategoriesIfNeeded, updatedCategoryItems } from './cases.js';
import * as cases from './cases.js';
import * as user from './user.js';
import { currentCategorySelector, currentItemSelector } from '../reducers/categories.js';

export const UPDATE_LOCATION = 'UPDATE_LOCATION';
export const RECEIVE_LAZY_RESOURCES = 'RECEIVE_LAZY_RESOURCES';
export const SET_ANNOUNCER_LABEL = 'SET_ANNOUNCER_LABEL';
export const CLEAR_ANNOUNCER_LABEL = 'CLEAR_ANNOUNCER_LABEL';
export const CLOSE_MODAL = 'CLOSE_MODAL';
export const UPDATE_NETWORK_STATUS = 'UPDATE_NETWORK_STATUS';
export const CLOSE_SNACKBAR = 'CLOSE_SNACKBAR';
export const UPDATE_DRAWER_STATE = 'UPDATE_DRAWER_STATE';
export const API_CONNECTED = 'API_CONNECTED';
export const UPDATED_CATEGORY_ITEMS = 'UPDATED_CATEGORY_ITEMS';

export const reloadCategory = () => async (dispatch, getState) => {
  let state = getState();
  const page = state.app.page;
  if (['category'].indexOf(page) === -1) {
    return;
  }

  let categoryName = state.app.categoryName;
  let category = currentCategorySelector(state);

  if (category) {
    await dispatch(fetchCategoryItemsIfNeeded(category));
    state = getState();
    category = currentCategorySelector(state);
    switch (page) {
      case 'category':
        dispatch(announceLabel(`${categoryName}, loaded`));
        return;
      case 'detail':
        const item = currentItemSelector(state);
        if (item) {
          dispatch(announceLabel(`${item.name}, loaded`));
          return;
        }
        break;
    }
  }

  dispatch(announceLabel(`Page not found`));
  dispatch({
    type: UPDATE_LOCATION,
    page: '404',
  });
};

export const updateLocation = (location) => async (dispatch, getState) => {
  const path = window.decodeURIComponent(location.pathname);
  const splitPath = (path || '').slice(1).split('/');
  let page = splitPath[0];
  let caseId = null;
  let caseName = null;
  let categoryName = null;
  let itemName = null;
  let section = null;
  await dispatch(fetchCategoriesIfNeeded());
  switch (page) {
    case '':
      page = 'home';
      dispatch(announceLabel(`Home, loaded`));
      break;
    case 'cart':
      await import('../components/shop-cart.js');
      dispatch(announceLabel(`Cart, loaded`));
      break;
    case 'checkout':
      await import('../components/shop-checkout.js');
      dispatch(announceLabel(`Checkout, loaded`));
      break;
    case 'category':
      categoryName = splitPath[1];
      await import('../components/category-list.js');
      break;
    case 'detail':
      categoryName = splitPath[1];
      itemName = splitPath[2];
      await import('../components/shop-detail.js');
      break;
    case 'case':
    case 'free_daily':
      caseId = splitPath[1];
      caseName = splitPath[2];
      await import('../components/case-detail.js');
      break;
    case 'user':
      section = splitPath[1];
      await import(`../components/user-${section}.js`);
      break;
    default:
      page = '404';
      dispatch(announceLabel(`Page not found`));
  }

  dispatch({
    type: UPDATE_LOCATION,
    page,
    caseId,
    caseName,
    categoryName,
    itemName
  });

  await dispatch(reloadCategory());

  const lazyLoadComplete = getState().app.lazyResourcesLoaded;
  // load lazy resources after render and set `lazyLoadComplete` when done.
  if (!lazyLoadComplete) {
    requestAnimationFrame(async () => {
      await import('../components/lazy-resources.js');
      dispatch({
        type: RECEIVE_LAZY_RESOURCES
      });
    });
  }
};

export const connectApi = (api) => async (dispatch, getState) => {
  window.socket = await io.connect(api);

  window.socket.reconnect = function () {
    console.log('reconnecting to api...');
    this.disconnect();
    this.connect();
  }

  let state = getState();

  socket.emit('cases');

  socket.on('cases loaded', (new_cases) => {

    let old_case_hash = state.app.cases_json;

    // no change to cases
    if(old_case_hash == new_cases) return;

    // compare to current val and if diff then update store
    dispatch(cases.cases_updated(new_cases));
  });

  socket.on('reconnect', data => socket.reconnect());

  socket.on('login...', userInfo => dispatch(user.userLogin(userInfo)));

  socket.on('updatedUserSettings', settings => dispatch(user.updatedUserSettings(settings)));

  socket.on('openCase', info => dispatch(cases.openCase(info)));

  socket.on('auth client', () => window.open(`https://lootdrop.app:9090/auth/steam`, 'lootdrop.app login', 'width=300,height=250'));

  return {
    type : API_CONNECTED,
    socket
  }

}

const setAnnouncerLabel = (label) => {
  return {
    type: SET_ANNOUNCER_LABEL,
    label
  };
};

const clearAnnouncerLabel = () => {
  return {
    type: CLEAR_ANNOUNCER_LABEL
  };
};

let announcerTimer = 0;

export const announceLabel = (label) => (dispatch) => {
  dispatch(clearAnnouncerLabel());
  // Debounce announcements.
  clearTimeout(announcerTimer);
  announcerTimer = setTimeout(() => {
    dispatch(setAnnouncerLabel(label));
  }, 300);
};

export const closeModal = () => {
  return {
    type: CLOSE_MODAL
  };
};

let snackbarTimer = 0;

export const updateNetworkStatus = (offline) => (dispatch, getState) => {
  const prevOffline = getState().app.offline;
  dispatch({
    type: UPDATE_NETWORK_STATUS,
    offline
  });
  clearTimeout(snackbarTimer);
  snackbarTimer = setTimeout(() => dispatch({ type: CLOSE_SNACKBAR }), 3000);

  if (!offline && prevOffline) {
    dispatch(reloadCategory());
  }
};

export const updateDrawerState = (opened) => {
  return {
    type: UPDATE_DRAWER_STATE,
    opened
  };
};
