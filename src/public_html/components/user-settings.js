/**
 * @license
 * Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PageViewElement } from './page-view-element.js';
import { html } from '@polymer/lit-element';
import { repeat } from 'lit-html/directives/repeat.js';
import { shopCommonStyle } from './shop-common-style.js';
import { shopButtonStyle } from './shop-button-style.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';
import { shopSelectStyle } from './shop-select-style.js';
import './shop-image.js';
import './shop-list-item.js';

import { store } from '../store.js';
import * as user from '../actions/user.js';
import { connect } from 'pwa-helpers/connect-mixin.js';

class UserSettings extends connect(store)(PageViewElement) {
  render() {
    return html `
    <div class="detail" has-content="">
        <h1>Settings</h1>
        <div class="price">$45.90</div>
        <div class="pickers">
          <shop-select>
            <label id="sizeLabel" prefix="">Offline Notifications</label>
            <select id="sizeSelect" aria-labelledby="sizeLabel">
              <option value="S">off</option>
              <option value="M" selected="">on</option>
            </select>
            <shop-md-decorator aria-hidden="true">
              <shop-underline></shop-underline>
            </shop-md-decorator>
          </shop-select>
        </div>
        <div class="description">
          <h2>Description</h2>
          <p id="desc">
            Brighten up your commute on gloomy days. This lightweight jacket features a subtle grid texture and a punch of bright pink at each side panel.
            <div><br></div>
            <div>Features:</div>
            <div>
            <ul>
              <li>${(this._user.settings) ? this._user.settings.bio : 'no user'}</li>
            </ul>
            </div>
          </p>
        </div>
        <shop-button @click="${this._saveSettings}" responsive="">
          <button aria-label="Add this item to cart">Save</button>
        </shop-button>
      </div>
    `;
  }

  static get properties() { return {
    _user : {type : Object}
  }}

  stateChanged(state) {
    // create a selector for this so we arent always updating
    this._user = state.user;
  }

  _categoryUpdated(items) {
    store.dispatch( () => 'CATEGORY UPDATED', items);
  }

  // look at selected settings, create an obj out of it and send it to server to be saved then resotored
  _saveSettings(e) {
    console.log('saving user settings');

    store.dispatch(user.saveSettings({
      email : 'king.zan@hotmail.com',
      offline_notifications : false,
      public_inventory : true,
      lang : 'en',
      bio : `Hi i\'m ${'Alex'}`
    }));
  }

}

customElements.define('user-settings', UserSettings);
