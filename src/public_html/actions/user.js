export const UPDATE = 'USER';
export const UPDATING_SETTINGS = 'UPDATING_SETTINGS';
export const UPDATED_SETTINGS = 'UPDATED_SETTINGS';


export const userLogin = (userInfo) => async (dispatch, getState) => {
  let user = userInfo;

  console.log('user info');

  console.log(user);

  dispatch({
    type : UPDATE,
    user
  })
}


export const saveSettings = (newSettings) => async (dispatch, getState) => {
  window.socket.emit('updateUserSettings', newSettings);
  dispatch({
    type : UPDATING_SETTINGS
  })
}

export const updatedUserSettings = (newSettings) => async(dispatch, getState) => {
  dispatch({
    type : UPDATED_SETTINGS,
    settings : newSettings
  })
}
