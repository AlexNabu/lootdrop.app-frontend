/**
 * @license
 * Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PageViewElement } from './page-view-element.js';
import { html } from '@polymer/lit-element';
import { repeat } from 'lit-html/directives/repeat.js';
import { shopCommonStyle } from './shop-common-style.js';
import { shopButtonStyle } from './shop-button-style.js';
import * as cases from '../actions/cases.js';
import './shop-image.js';
import './shop-list-item.js';

import { store } from '../store.js';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { currentCategorySelector, currentItemSelector, caseSelector} from '../reducers/cases.js';

class ShopHome extends connect(store)(PageViewElement) {
  render() {
    const _failure = this._failure;
    return html`
    ${shopCommonStyle}
    ${shopButtonStyle}
    <style>

      .hero-image {
        position: relative;
        height: 320px;
        overflow: hidden;
        margin-bottom: 32px;
      }

      .grid {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: space-between;
        margin: 0 10px 32px 10px;
        padding: 0;
        list-style: none;
      }

      .grid li {
        -webkit-flex: 1 1;
        flex: 1 1;
        -webkit-flex-basis: 33%;
        flex-basis: 33%;
        max-width: 33%;
      }

      .grid a {
        display:block;
        text-decoration: none;
      }

      @media (max-width: 767px) {
        .hero-image {
          display: none;
        }

        .grid  li {
          -webkit-flex-basis: 50%;
          flex-basis: 50%;
          max-width: 50%;
        }
      }

    </style>


    ${!this._failure ? html`
      <ul class="grid">
      <li>
        <a href="/free_daily">
          <shop-list-item .case="${this._item}"></shop-list-item>
        </a>
      </li>

      <li>
        <a href="/free_daily">
          <shop-list-item .case="${this._item}"></shop-list-item>
        </a>
      </li>


      <li>
        <a href="/free_daily">
          <shop-list-item .case="${this._item}"></shop-list-item>
        </a>
      </li>

      <shop-button><button @click="${this._openCase}">Open Case</button></shop-button>


    ` : html`
      <shop-network-warning></shop-network-warning>
    `}`;
  }

  static get properties() { return {
    _item : {type : Object},

    _failure : {type : Boolean}
  }}

  stateChanged(state) {
    if(state.app.page == 'free_daily') {
      this._item = state.app.freeDailyCase || {};
      this._freeDaily = true;
      return;
    }

    this._item = caseSelector(state) || {};
    this._freeDaily = false;

    console.log(this._item);
  }

  _getListItems(items) {
    // Return placeholder items when the items haven't loaded yet.
    return items && items.lit ? items.lit.items : [{},{},{},{},{},{},{},{},{},{}];
  }

  _openCase(e) {
    if(this._freeDaily) return store.dispatch(cases.openningFreeDaily());

    store.dispatch(cases.openningCase(this._item.id));
  }

}

customElements.define('case-detail', ShopHome);
